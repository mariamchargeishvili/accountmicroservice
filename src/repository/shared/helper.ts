
interface IPaginationType {
    offset: number;
    limit: number;
}

export const paginate = (page: number, pageSize: number): IPaginationType => {
    const offset = page * pageSize;
    const limit =  pageSize;
    return {
        offset,
        limit,
    };
};
