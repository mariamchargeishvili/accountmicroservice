import { Op, Sequelize } from "sequelize";
import { Document, DOCUMENT_STATUSES } from "shared_models/dist/models/account/document";
import { Account as AccountDbModel } from "../database/account.model";
import { Document as DocumentDbModel } from "../database/document.model";
import { paginate } from "./shared/helper";
class DocumentRepository {

    public createAll(documents: Document[]) {
        return DocumentDbModel.bulkCreate(documents);
    }

    public updateDocumentByGroupKey(groupKey: string, document: Document) {
        return DocumentDbModel.update(document, { where: { groupKey } });
    }

    public getAccountDocuments(accountId, withFiles: boolean, excludeRejected = false): any {

        const where: any = { accountId };

        if (excludeRejected) { where.status = { [Op.not]: DOCUMENT_STATUSES.REJECTED }; }

        if (withFiles) { return DocumentDbModel.findAll({ where }); } else {
            return DocumentDbModel.findAll({
                where,
                attributes: [
                    "groupKey",
                    [Sequelize.fn("max", Sequelize.col("accountId")), "accountId"],
                    [Sequelize.fn("max", Sequelize.col("documentId")), "documentId"],
                    [Sequelize.fn("max", Sequelize.col("documentType")), "documentType"],
                    [Sequelize.fn("max", Sequelize.col("documentIssueDate")), "documentIssueDate"],
                    [Sequelize.fn("max", Sequelize.col("documentExpirationDate")), "documentExpirationDate"],
                    [Sequelize.fn("max", Sequelize.col("comment")), "comment"],
                    [Sequelize.fn("max", Sequelize.col("status")), "status"],
                ],
                group: ["groupKey"],
            });
        }
    }

    public async getVerificationQueue(filters, pagination: { page: number, pageSize: number }) {
        const documentsWhere: any = {};

        if (filters.accountId) { documentsWhere.accountId = filters.accountId; }
        if (filters.documentStatus) { documentsWhere.status = filters.documentStatus; }
        if (filters.from) { documentsWhere.createdAt = { [Op.gt]: filters.from }; }
        if (filters.to) {
            documentsWhere.createdAt =
                Object.assign(documentsWhere.createdAt || {}, { [Op.lt]: filters.to });
        }

        const accountsWhere: any = {};
        if (filters.personalId) { accountsWhere.personalId = filters.personalId; }

        const paginationData = paginate(pagination.page, pagination.pageSize);

        const data = await DocumentDbModel.findAndCountAll({
            where: documentsWhere,
            attributes: [
                "groupKey",
                [Sequelize.fn("max", Sequelize.col("document.createdAt")), "documentCreatedAt"],
                [Sequelize.fn("max", Sequelize.col("documentId")), "documentId"],
                [Sequelize.fn("max", Sequelize.col("documentType")), "documentType"],
                [Sequelize.fn("max", Sequelize.col("document.status")), "documentStatus"],
                [Sequelize.fn("max", Sequelize.col("document.accountId")), "accountId"],
                [Sequelize.fn("max", Sequelize.col("document.comment")), "documentComment"],
            ],
            group: ["groupKey"],
            limit: paginationData.limit,
            offset: paginationData.offset,
            order: [[Sequelize.fn("max", Sequelize.col("document.createdAt")), "DESC"]],
            raw: true,
            include: [{
                model: AccountDbModel,
                where: accountsWhere,
                attributes: [
                    [Sequelize.fn("max", Sequelize.col("personalId")), "personalId"],
                ],
            }],
        });
        return { rows: data.rows, count: data.count.length };
    }

    public async getExpiredDocuments() {
        return await DocumentDbModel.findAll(
            {
                where: {
                    documentExpirationDate: { [Op.lt]: new Date() },
                    // status: { [Op.not]: DOCUMENT_STATUSES.EXPIRED }
                    status: DOCUMENT_STATUSES.VERIFIED,
                },
                attributes: [
                    "groupKey",
                    [Sequelize.fn("max", Sequelize.col("accountId")), "accountId"],
                    [Sequelize.fn("max", Sequelize.col("documentExpirationDate")), "documentExpirationDate"],
                ],
                group: ["groupKey"],
            });
    }

    public async updateDocumentStatus(status: DOCUMENT_STATUSES, groupKey?: string, id?: number) {
        if (groupKey) { return DocumentDbModel.update({ status }, { fields: ["status"], where: { groupKey } }); }
        if (id) { return DocumentDbModel.update({ status }, { fields: ["status"], where: { id } }); }
    }

}
export { DocumentRepository };
