import { Op } from "sequelize";
import { Account } from "shared_models/dist/models/account/account";
import { Account as AccountDbModel } from "../database/account.model";

class AccountRepository {

  public create(account: Account) {
    return AccountDbModel.create(account);
  }

  public findByKeyAndValue(key: string, value: string) {
    return AccountDbModel.findOne({
      where: { [key]: value },
    });
  }

  public findById(id: number) {
    return AccountDbModel.findByPk(id);
  }

  public getAccountList(ids: number[], fields: string[]) {
    return AccountDbModel.findAll({
      attributes: fields,
      where: {
        id: { [Op.in]: ids },
      },
    });
  }

  public update(accountId: number, account: Account) {
    return AccountDbModel.update(account, { where: { id: accountId } });
  }
}

export { AccountRepository };
