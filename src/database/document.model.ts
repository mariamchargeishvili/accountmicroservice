import { DataTypes, Model } from "sequelize";
import { DOCUMENT_STATUSES, DOCUMENT_TYPES, FILE_TYPES } from "shared_models/dist/models/account/document";
import { enumsToArray } from "../helpers";

class Document extends Model { }

const DocumentModel = {
    accountId: {
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    documentId: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    documentType: {
        type: DataTypes.ENUM,
        values: enumsToArray(DOCUMENT_TYPES),
        allowNull: false,
    },
    documentIssueDate: {
        type: DataTypes.DATEONLY,
        allowNull: false,
        unique: false,
    },
    documentExpirationDate: {
        type: DataTypes.DATEONLY,
        allowNull: false,
        unique: false,
    },
    comment: {
        type: DataTypes.TEXT,
        allowNull: true,
        unique: false,
    },
    status: {
        type: DataTypes.ENUM,
        values: enumsToArray(DOCUMENT_STATUSES),
        allowNull: false,
        defaultValue: "VERIFICATION_PENDING",
    },
    fileName: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    fileType: {
        type: DataTypes.ENUM,
        values: enumsToArray(FILE_TYPES),
        allowNull: false,
    },
    groupKey: {
        type: DataTypes.UUID,
        allowNull: false,
    },
};

export { DocumentModel, Document };
