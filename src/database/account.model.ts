import { DataTypes, Model } from "sequelize";
import { ACCOUNT_STATUSES, COUNTRIES, GENDER } from "shared_models/dist/models/account/account";
import { enumsToArray } from "../helpers";

class Account extends Model { }

const AccountModel = {
  username: {
    type: DataTypes.STRING,
    allowNull: true,
    unique: true,
  },
  email: {
    type: DataTypes.STRING,
    allowNull: true,
    unique: true,
  },
  phoneNumber: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true,
  },
  personalId: {
    type: DataTypes.STRING,
    allowNull: true,
    unique: true,
  },
  abUserId: {
    type: DataTypes.INTEGER,
    allowNull: true,
    unique: true,
  },
  firstName: {
    type: DataTypes.STRING,
    allowNull: true,
  },
  lastName: {
    type: DataTypes.STRING,
    allowNull: true,
  },
  birthDate: {
    type: DataTypes.DATEONLY,
    allowNull: true,
  },
  gender: {
    type: DataTypes.ENUM,
    values: enumsToArray(GENDER),
    allowNull: true,
  },
  citizenship: {
    type: DataTypes.ENUM,
    values: enumsToArray(COUNTRIES),
    allowNull: true,
  },
  city: {
    type: DataTypes.STRING,
    allowNull: true,
  },
  status: {
    type: DataTypes.ENUM,
    values: enumsToArray(ACCOUNT_STATUSES),
    allowNull: false,
    defaultValue: "UNVERIFIED",
  },
  isPoliticallyExposedPerson: {
    type: DataTypes.BOOLEAN,
    allowNull: true,
  },
  isEntrepreneur: {
    type: DataTypes.BOOLEAN,
    allowNull: true,
  },
  comment: {
    type: DataTypes.TEXT,
    allowNull: true,
    unique: false,
  },
  isBlocked: {
    type: DataTypes.BOOLEAN,
    allowNull: true,
  },
  additionalInfo: {
    type: DataTypes.JSONB,
  },
};

export { AccountModel, Account };
