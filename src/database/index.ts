import Sequelize from "sequelize";
import { Account, AccountModel } from "./account.model";
import { Document, DocumentModel } from "./document.model";

const sequelize = new Sequelize.Sequelize(
  {
    database: process.env.DB_NAME,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    host: process.env.DB_HOST,
    port: Number(process.env.DB_PORT),
    dialect: "postgres",
    logging: false,
  });

Account.init(AccountModel, { sequelize, modelName: "account" });
Document.init(DocumentModel, { sequelize, modelName: "document" });
Document.belongsTo(Account);
// const account = sequelize.define('account', AccountModel)
// const document = sequelize.define('document', DocumentModel)
// document.belongsTo(account, { foreignKey: 'accountId' });

const getAccountModels = async () => {
  const dbInstance = await sequelize.sync({ force: false }).catch(console.log);
  return dbInstance.models;
};

export { getAccountModels as AccountModels };
