const enumsToArray = (e): [string] | any => Object.values(e).filter((x) => typeof x === "string");

export { enumsToArray };
