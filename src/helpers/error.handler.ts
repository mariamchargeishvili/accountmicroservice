import { ServiceError } from "shared_models/dist/models/app/srvice-error/service-error";
import { ERRORS } from "./enums";

const errorHandler = (err) => {
  const error: ServiceError = {
    name: "SYSTEM_ERROR",
    statusCode: 500,
  };

  console.log("error handler: ", err);

  if (err.code === "ERR_ASSERTION") {
    error.type = "SYSTEM_ERROR";
    error.name = "WRONG_PROCEDURE_NAME";
  } else if (err.isJoi) {
    error.type = "VALIDATION_ERROR";
    error.name = "VALIDATION_ERROR";
    error.description = err.name;
    error.statusCode = 400;
    error.additionalData = err.details;
  } else if (err.name === "SequelizeUniqueConstraintError") {
    error.type = "VALIDATION_ERROR";
    error.name = "UNIQUE_KEY_ERROR";
    error.description = "UNIQUE_KEY_ERROR";
    error.statusCode = 409;
    error.additionalData = err.errors;
  } else if (err.level === "API_ERROR") {
    error.name = err.name;
    error.type = err.type;
    error.description = err.message;
    error.statusCode = err.statusCode;
    error.additionalData = err.additionalData;
  } else {
    error.type = "SYSTEM_ERROR";
    error.name = "SYSTEM_ERROR";
    error.statusCode = 500;
    error.additionalData = err.toString();
  }

  return error;

};
export { errorHandler };
