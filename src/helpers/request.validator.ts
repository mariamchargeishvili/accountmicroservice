import joi from "joi";
import { procedures } from "shared_models";

const schemas = procedures.account;

const validateRequest = (procedure, data) => {
  return joi.validate(data, schemas[procedure]);
};

export { validateRequest };
