import { Document, DOCUMENT_STATUSES } from "shared_models/dist/models/account/document";
import { DocumentRepository } from "../repository/document.repository";

class DocumentService {
    constructor(private documentRepository: DocumentRepository) { }

    public async createAll(documents: Document[]) {
        return await this.documentRepository.createAll(documents);
    }

    public async updateDocumentByGroupKey(groupKey: string, document: Document) {
        return await this.documentRepository.updateDocumentByGroupKey(groupKey, document);
    }

    public async getAccountDocuments(accountId, withFiles: boolean, excludeRejected = false): Promise<Document[]> {
        return await this.documentRepository.getAccountDocuments(accountId, withFiles, excludeRejected);
    }

    public async getVerificationQueue(filters, pagination: { page: number, pageSize: number }): Promise<any> {
        return await this.documentRepository.getVerificationQueue(filters, pagination);
    }

    public async getExpiredDocuments(): Promise<Document[]> {
        return await this.documentRepository.getExpiredDocuments();
    }

    public async updateDocumentStatus(status: DOCUMENT_STATUSES, groupKey?: string, documentId?: number) {
        return await this.documentRepository.updateDocumentStatus(status, groupKey, documentId);
    }

}

export { DocumentService };
