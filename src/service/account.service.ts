import Promise from "bluebird";
import { Account } from "shared_models/dist/models/account/account";
import { ERRORS } from "../helpers/enums";
import { AccountRepository } from "../repository/account.repository";

class AccountService {
  constructor(private accountRepository: AccountRepository) { }

  public async create(account: Account) {
    return await this.accountRepository.create(account);
  }

  public async setBlockStatus(id: number, isBlocked: boolean, comment: string) {
    const account = this.getAccountById(id);
    if (!account) {
      throw { level: "API_ERROR", name: ERRORS.ACCOUNT_NOT_FOUND, statusCode: 400 };
    }
    await this.accountRepository.update(id, { comment });
    return await this.accountRepository.update(id, { isBlocked });
  }

  public async setAccountInfo(accountId: number, account) {
    const existingAccount = await this.getAccountById(account.id);
    if (!existingAccount) {
      throw { level: "API_ERROR", name: ERRORS.ACCOUNT_NOT_FOUND, statusCode: 400 };
    }

    return await this.accountRepository.update(accountId, account);
  }

  public async findByKeyAndValue(key: string, value: string) {
    return await this.accountRepository.findByKeyAndValue(key, value);
  }

  public async setUsername(id: number, username: string, restrictReSet = false) {
    const account = this.getAccountById(id);
    const usernameIsTakenPromise = this.accountRepository.findByKeyAndValue("username", username);

    if (!account) {
      throw { level: "API_ERROR", name: ERRORS.ACCOUNT_NOT_FOUND, statusCode: 400 };
    }

    // join promises for parallel execution
    await Promise.join(account, usernameIsTakenPromise).then((results) => {
      const accountById = results[0];
      const accountByUserName = results[1];
      // account does not exist
      if (!accountById) {
        throw { level: "API_ERROR", name: ERRORS.ACCOUNT_NOT_FOUND, statusCode: 400 };
      }

      if (restrictReSet) { // username already set
        if (accountById.username) {
          throw { level: "API_ERROR", name: ERRORS.USERNAME_ALREADY_SET, statusCode: 400 };
        }
      }
      // username is in use by somebody else
      if (accountByUserName && accountByUserName.id !== id) {
        throw { level: "API_ERROR", name: ERRORS.USERNAME_UNAVAILABLE, statusCode: 400 };
      }
    });

    return await this.accountRepository.update(id, { username });
  }

  public async updatePhoneNumber(id: number, phoneNumber: string) {
    const account = this.getAccountById(id);
    const phoneIsTakenPromise = this.accountRepository.findByKeyAndValue("phoneNumber", phoneNumber);

    if (!account) {
      throw { level: "API_ERROR", name: ERRORS.ACCOUNT_NOT_FOUND, statusCode: 400 };
    }

    // join promises for parallel execution
    await Promise.join(account, phoneIsTakenPromise).then((results) => {
      // account does not exist
      if (!results[0]) {
        throw { level: "API_ERROR", name: ERRORS.ACCOUNT_NOT_FOUND, statusCode: 400 };
      }

      // phone is in use by somebody else
      if (results[1] && results[1].id !== id) {
        throw { level: "API_ERROR", name: ERRORS.PHONE_UNAVAILABLE, statusCode: 400 };
      }
    });

    return await this.accountRepository.update(id, { phoneNumber });
  }

  public async updatePersonalId(id: number, personalId: string) {
    const account = this.getAccountById(id);
    const personalIdIsTakenPromise = this.accountRepository.findByKeyAndValue("personalId", personalId);

    if (!account) {
      throw { level: "API_ERROR", name: ERRORS.ACCOUNT_NOT_FOUND, statusCode: 400 };
    }

    // join promises for parallel execution
    await Promise.join(account, personalIdIsTakenPromise).then((results) => {
      // account does not exist
      if (!results[0]) {
        throw { level: "API_ERROR", name: ERRORS.ACCOUNT_NOT_FOUND, statusCode: 400 };
      }

      // phone is in use by somebody else
      if (results[1] && results[1].id !== id) {
        throw { level: "API_ERROR", name: ERRORS.PERSONAL_ID_UNAVAILABLE, statusCode: 400 };
      }
    });

    return await this.accountRepository.update(id, { personalId });
  }

  public async setEmail(id: number, email: string) {
    const account = this.getAccountById(id);
    const emailIsTakenPromise = this.accountRepository.findByKeyAndValue("email", email);

    if (!account) {
      throw { level: "API_ERROR", name: ERRORS.ACCOUNT_NOT_FOUND, statusCode: 400 };
    }

    // join promises for parallel execution
    await Promise.join(account, emailIsTakenPromise).then((results) => {
      // account does not exist
      if (!results[0]) {
        throw { level: "API_ERROR", name: ERRORS.ACCOUNT_NOT_FOUND, statusCode: 400 };
      }

      // email is in use by somebody else
      if (results[1] && results[1].id !== id) {
        throw { level: "API_ERROR", name: ERRORS.EMAIL_UNAVAILABLE, statusCode: 400 };
      }
    });

    return await this.accountRepository.update(id, { email });
  }

  public async setAccountStatus(id, status) {
    const account = await this.getAccountById(id);

    if (!account) {
      throw { level: "API_ERROR", name: ERRORS.ACCOUNT_NOT_FOUND, statusCode: 400 };
    }

    return await this.accountRepository.update(id, { status });
  }

  public async updateAdditionalInfo(id: number, data: object) {
    const account = await this.getAccountById(id);

    const additionalInfo = Object.assign(account.additionalInfo || {}, data);
    return await this.accountRepository.update(id, { additionalInfo });
  }

  public async getAccountById(id: number) {
    const account = await this.accountRepository.findById(id);
    if (!account) {
      throw { level: "API_ERROR", name: ERRORS.ACCOUNT_NOT_FOUND, statusCode: 400 };
    }

    return account;
  }

  public async getAccountList(ids: number[], fields: string[]) {
    const accountList = await this.accountRepository.getAccountList(ids, fields);

    if (!accountList) {
      throw { level: "API_ERROR", name: ERRORS.ACCOUNT_NOT_FOUND, statusCode: 400 };
    }

    return accountList;
  }
}

export { AccountService };
