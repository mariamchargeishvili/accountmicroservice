import bodyParser from "body-parser";
import express from "express";
import { bootstrap } from "../app";

import { AccountRoutes } from "./routes";

const start = async () => {
  try {
    const APP = await bootstrap();
    const Server = express();

    const accountRoutes = AccountRoutes(APP.accountController, APP.documentController);

    Server.use(bodyParser.json());
    Server.use(bodyParser.urlencoded({ extended: false }));
    Server.use(accountRoutes);
    Server.listen(process.env.SERVER_PORT);
    console.log(`AccountService started on ${process.env.SERVER_PORT}`);
  } catch (e) {
    console.log(e);
  }
};

start().catch(console.log);
