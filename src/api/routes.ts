import { Request, Response, Router } from "express";
import { AccountController } from "../controller/account.controller";
import { DocumentController } from "../controller/document.controller";
import { errorHandler } from "../helpers/error.handler";
import { validateRequest } from "../helpers/request.validator";

const AccountRoutes = (
  accountController: AccountController,
  documentController: DocumentController,
) => {
  const router = Router();

  router.post("/accounts", async (req: Request, res: Response) => {
    try {
      console.log(req);

      await validateRequest("create", req.body);
      const data = await accountController.create(req.body);
      return res.json({ data });
    } catch (e) {
      const service_error = errorHandler(e);
      return res.json({ service_error });
    }
  });

  router.put("/accounts/:id", async (req: Request, res: Response) => {
    try {
      await validateRequest("setUserInfo", req.body);
      const data = await accountController.setAccountInfo(req.params.id, req.body);
      return res.json({ data });
    } catch (e) {
      const service_error = errorHandler(e);
      return res.json({ service_error });
    }
  });

  router.get("/findByKeyAndValue/:key/:value", async (req: Request, res: Response) => {
    try {
      await validateRequest("findByKeyAndValue", req.params);
      const data = await accountController.findByKeyAndValue(
        req.params.key,
        req.params.value,
      );
      return res.json({ data });
    } catch (e) {
      const service_error = errorHandler(e);
      return res.json({ service_error });
    }
  });

  router.post("/getAccountList", async (req: Request, res: Response) => {
    try {
      await validateRequest("getAccountList", req.body);
      const data = await accountController.getAccountList(
        req.body.ids,
        req.body.fields,
      );
      return res.json({ data });
    } catch (e) {
      const service_error = errorHandler(e);
      return res.json({ service_error });
    }
  });

  router.post("/accounts/:id/username",
    async (req: Request, res: Response) => {
      try {
        const request = { id: req.params.id, username: req.body.username };
        await validateRequest("setUsername", request);
        const data = await accountController.setUsername(
          request.id,
          request.username,
          true,
        );
        return res.json({ data });
      } catch (e) {
        const service_error = errorHandler(e);
        return res.json({ service_error });
      }
    },
  );

  router.put("/accounts/:id/username",
    async (req: Request, res: Response) => {
      try {
        const request = { id: req.params.id, username: req.body.username };
        await validateRequest("updateUsername", request);
        const data = await accountController.setUsername(
          request.id,
          request.username,
        );
        // console.log(data);
        return res.json({ data });
      } catch (e) {
        const service_error = errorHandler(e);
        return res.json({ service_error });
      }
    },
  );

  router.put("/accounts/:id/phoneNumber",
    async (req: Request, res: Response) => {
      try {
        const request = { id: req.params.id, phoneNumber: req.body.phoneNumber };
        await validateRequest("updatePhoneNumber", request);
        const data = await accountController.updatePhoneNumber(
          request.id,
          request.phoneNumber,
        );
        return res.json({ data });
      } catch (e) {
        const service_error = errorHandler(e);
        return res.json({ service_error });
      }
    });

  router.put("/accounts/:id/personalId",
    async (req: Request, res: Response) => {
      try {
        const request = { id: req.params.id, personalId: req.body.personalId };
        await validateRequest("updatePersonalId", request);
        const data = await accountController.updatePersonalId(
          request.id,
          request.personalId,
        );
        return res.json({ data });
      } catch (e) {
        const service_error = errorHandler(e);
        return res.json({ service_error });
      }
    });

  router.put("/accounts/:id/emailStatus",
    async (req: Request, res: Response) => {
      try {
        const request = { id: req.params.id, status: req.body.status };
        await validateRequest("changeEmailStatus", request);
        const data = await accountController.updateEmailStatus(
          request.id,
          request.status,
        );
        return res.json({ data });
      } catch (e) {
        const service_error = errorHandler(e);
        return res.json({ service_error });
      }
    },
  );

  router.put("/accounts/:id/email",
    async (req: Request, res: Response) => {
      try {
        const request = { id: req.params.id, email: req.body.email };
        await validateRequest("setEmail", request);
        const data = await accountController.setEmail(
          request.id,
          request.email,
        );
        return res.json({ data });
      } catch (e) {
        const service_error = errorHandler(e);
        return res.json({ service_error });
      }
    });

  router.put("/accounts/:id/status",
    async (req: Request, res: Response) => {
      try {
        const request = { id: req.params.id, status: req.body.status };
        await validateRequest("setAccountStatus", request);
        const data = await accountController.setAccountStatus(
          request.id,
          request.status,
        );
        return res.json({ data });
      } catch (e) {
        const service_error = errorHandler(e);
        return res.json({ service_error });
      }
    });

  router.put("/accounts/:id/blockStatus",
    async (req: Request, res: Response) => {
      try {
        const request = { id: req.params.id, status: req.body.status, comment: req.body.comment };
        await validateRequest("setBlockStatus", request);
        const data = await accountController.setBlockStatus(
          request.id,
          request.status,
          request.comment,
        );
        return res.json({ data });
      } catch (e) {
        const service_error = errorHandler(e);
        return res.json({ service_error });
      }
    });

  router.post("/accountDocuments",
    async (req: Request, res: Response) => {
      try {
        console.log(req.body);
        await validateRequest("addDocument", req.body);
        const data = await documentController.createAll(req.body);
        return res.json({ data });
      } catch (e) {
        const service_error = errorHandler(e);
        return res.json({ service_error });
      }
    });

  router.put("/accountDocuments/:groupKey",
    async (req: Request, res: Response) => {
      try {
        await validateRequest("updateDocument", req.body);
        const data = await documentController.updateDocumentByGroupKey(req.params.groupKey, req.body);
        return res.json({ data });
      } catch (e) {
        const service_error = errorHandler(e);
        return res.json({ service_error });
      }
    });

  router.put("/accountDocuments/expire",
    async (req: Request, res: Response) => {
      try {
        console.log("updateExpiredDocuments executed");
        accountController.checkDocumentExpiration();
        return res.end();
      } catch (e) {
        const service_error = errorHandler(e);
        return res.json({ service_error });
      }
    });

  router.post(
    "/checkAccountStatusChangeOnDocumentUpload",
    async (req: Request, res: Response) => {
      try {
        await validateRequest("accountId", req.body);
        const data = await accountController.checkAccountStatusChangeOnDocumentUpload(req.body.accountId);
        return res.json({ data });
      } catch (e) {
        const service_error = errorHandler(e);
        return res.json({ service_error });
      }
    });

  router.get(
    "/accountDocuments/:accountId",
    async (req: Request, res: Response) => {
      try {
        const withFiles = req.query.withFiles === "true";
        const excludeRejected = req.query.excludeRejected === "true";
        const data = await documentController.getAccountDocuments(req.params.accountId, withFiles, excludeRejected);
        return res.json({ data });
      } catch (e) {
        const service_error = errorHandler(e);
        return res.json({ service_error });
      }
    });

  router.get(
    "/verificationQueue",
    async (req: Request, res: Response) => {
      try {

        const filters = {
          from: req.query.from,
          to: req.query.to,
          accountId: req.query.accountId,
          personalId: req.query.personalId,
          documentStatus: req.query.documentStatus,
        };

        await validateRequest("verificationQueue", filters);
        const pagination = {
          page: Number(req.query.page) || 0,
          pageSize: Number(req.query.pageSize) || 10,
        };

        const data = await documentController.getVerificationQueue(filters, pagination);
        return res.json({ data });
      } catch (e) {
        const service_error = errorHandler(e);
        return res.json({ service_error });
      }
    });

  return router;
};

export { AccountRoutes };
