import { Account, ACCOUNT_STATUSES } from "shared_models/dist/models/account/account";
import { DOCUMENT_STATUSES } from "shared_models/dist/models/account/document";
import { ERRORS } from "../helpers/enums";
import { AccountService } from "../service/account.service";
import { DocumentService } from "../service/document.service";

class AccountController {
  constructor(private accountService: AccountService, private documentService: DocumentService) { }

  public async create(account: Account) {
    return this.accountService.create(account);
  }
  public async setAccountInfo(accountId: number, account: Account) {
    return this.accountService.setAccountInfo(accountId, account);
  }
  public async findByKeyAndValue(key: string, value: string) {
    return this.accountService.findByKeyAndValue(key, value);
  }
  public async getAccountList(ids: number[], fields: string[]) {
    return this.accountService.getAccountList(ids, fields);
  }
  public async setUsername(id: number, username: string, restrictReSet = false) {
    return this.accountService.setUsername(id, username, restrictReSet);
  }
  public async updatePhoneNumber(id: number, phoneNumber: string) {
    return this.accountService.updatePhoneNumber(id, phoneNumber);
  }
  public async updatePersonalId(id: number, personalId: string) {
    return this.accountService.updatePersonalId(id, personalId);
  }
  public async updateEmailStatus(id: number, status: string) {
    return this.accountService.updateAdditionalInfo(id, {
      emailStatus: status,
    });
  }
  public async setEmail(id: number, email: string) {
    if (await this.accountService.setEmail(id, email)) {
      return await this.updateEmailStatus(id, "UNVERIFIED");
    }
  }
  public async setAccountStatus(id: number, status: ACCOUNT_STATUSES) {
    return this.accountService.setAccountStatus(id, status);
  }
  public async setBlockStatus(id: number, isBlocked: boolean, comment: string) {
    return this.accountService.setBlockStatus(id, isBlocked, comment);
  }

  public async checkDocumentExpiration() {
    const expiredDocuments = await this.documentService.getExpiredDocuments();
    expiredDocuments.forEach(async (document) => {
      await this.documentService.updateDocumentStatus(DOCUMENT_STATUSES.EXPIRED, document.groupKey);

      const accountDocs = await this.documentService.getAccountDocuments(document.accountId, false);

      if (!accountDocs.find((accountDoc) => accountDoc.status === DOCUMENT_STATUSES.VERIFIED)) {
        if (accountDocs.find((accountDoc) => accountDoc.status === DOCUMENT_STATUSES.VERIFICATION_PENDING)) {
          await this.accountService.setAccountStatus(document.accountId, ACCOUNT_STATUSES.VERIFICATION_PENDING);
        } else { await this.accountService.setAccountStatus(document.accountId, ACCOUNT_STATUSES.EXPIRED); }
      }
    });
  }
  public async checkAccountStatusChangeOnDocumentUpload(accountId) {
    const account: Account = await this.accountService.findByKeyAndValue("id", accountId);
    if (!account) { throw new Error(ERRORS.ACCOUNT_NOT_FOUND); }
    const newStatus = await this.calculateAccountStatusByDocuments(accountId);
    if (account.status !== newStatus) {
      this.accountService.setAccountStatus(accountId, newStatus);
    }
    return newStatus;
  }

  public async calculateAccountStatusByDocuments(accountId): Promise<ACCOUNT_STATUSES> {
    const documents = await this.documentService.getAccountDocuments(accountId, false);
    if (documents.find((document) => document.status === DOCUMENT_STATUSES.VERIFIED)) {
      return ACCOUNT_STATUSES.VERIFIED;
    } else if (documents.find((document) => document.status === DOCUMENT_STATUSES.VERIFICATION_PENDING)) {
      return ACCOUNT_STATUSES.VERIFICATION_PENDING;
    } else if (documents.find((document) => document.status === DOCUMENT_STATUSES.EXPIRED)) {
      return ACCOUNT_STATUSES.EXPIRED;
    } else { return ACCOUNT_STATUSES.UNVERIFIED; }
  }
}

export { AccountController };
