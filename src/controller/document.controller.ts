import { Document } from "shared_models/dist/models/account/document";
import { DocumentService } from "../service/document.service";

class DocumentController {
    constructor(private documentService: DocumentService) { }

    public async createAll(documents: Document[]) {
        return this.documentService.createAll(documents);
    }

    public async updateDocumentByGroupKey(groupKey: string, document: Document) {
        return this.documentService.updateDocumentByGroupKey(groupKey, document);
    }

    public async getAccountDocuments(accountId: number, withFiles: boolean, excludeRejected = false) {
        return this.documentService.getAccountDocuments(accountId, withFiles, excludeRejected);
    }

    public async getVerificationQueue(filters, pagination: { page: number, pageSize: number }) {
        return this.documentService.getVerificationQueue(filters, pagination);
    }
}

export { DocumentController };
