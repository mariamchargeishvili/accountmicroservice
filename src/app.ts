import { AccountController } from "./controller/account.controller";
import { DocumentController } from "./controller/document.controller";
import { AccountModels } from "./database";
import { AccountRepository } from "./repository/account.repository";
import { DocumentRepository } from "./repository/document.repository";
import { AccountService } from "./service/account.service";
import { DocumentService } from "./service/document.service";

const bootstrap = async () => {
  await AccountModels();
  const documentRepository = new DocumentRepository();
  const documentService = new DocumentService(documentRepository);
  const documentController = new DocumentController(documentService);

  const accountRepository = new AccountRepository();
  const accountService = new AccountService(accountRepository);
  const accountController = new AccountController(accountService, documentService);

  return {
    accountController,
    documentController,
  };
};

export { bootstrap };
