**Account Service**
----

**Create New User Account**
    called when the user registers

* **URL**

    /accounts

* **Method:**

  `POST`
  
* **Data Params**
    <br>
    **Required:**
    <br>
    ```
    phoneNumber: string
    ```



* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```
    {
        "data": {
            "status": "UNVERIFIED",
            "id": 17,
            "phoneNumber": "123123123",
            "updatedAt": "2019-06-06T12:01:12.559Z",
            "createdAt": "2019-06-06T12:01:12.559Z",
            "username": null,
            "email": null,
            "personalId": null,
            "abUserId": null,
            "firstName": null,
            "lastName": null,
            "birthDate": null,
            "gender": null,
            "citizenship": null,
            "city": null,
            "isPoliticallyExposedPerson": null,
            "isEntrepreneur": null,
            "comment": null,
            "isBlocked": null,
            "additionalInfo": null
        }
    }
    ```
 
* **Error Response:**

  * **Code:** 400 BAD REQUEST <br />
    **Content:** 
    ```
    {
        "service_error": {
            "type": "VALIDATION_ERROR",
            "description": "ValidationError" | "UNIQUE_KEY_ERROR",
            "httpCode": 400 | 409,
            "additionalData": [
                //joi validation info here
            ]
        }
    }
    ```

-----------------------------


**Set Account Info**
----

* **URL**

    /accounts/:id

* **Method:**

  `PUT`

* **URL Params**
    <br>
    **Required:**
    <br>
    ```
       id = [number] -- account id
    ```

  
* **Data Params**
    <br>
    **Required:**
    <br>
    ```
    id: number
    firstName: string
    lastName: string
    birthDate: date
    gender: string
    citizenship: string
    city: string
    personalId: string
    isPoliticallyExposedPerson: boolean
    isEntrepreneur: boolean
    ```

    <br>
* **Optional:**
    <br>
    ```
    comment: string
    additionalInfo: object
    ```

* **Request example**

    ```
    {
        "id": "17",
        "firstName": "testo",
        "lastName": "testy",
        "birthDate": "10-10-2010",
        "gender": "FEMALE",
        "citizenship": "GEORGIA",
        "city": "Quteisi",
        "personalId": "11223344556",
        "isPoliticallyExposedPerson": "false",
        "isEntrepreneur": "false"
    }
    ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    ```
    {
        "data": [
            1
        ]
    }
    ```
 
* **Error Response:**

  * **Code:** 200 OK <br />
    **Content:** 

    ```
    {
        "service_error": {
            "type": "VALIDATION_ERROR",
            "description": "ValidationError",
            "httpCode": 400,
            "additionalData": [
                //joi validation info here
            ]
        }
    }
    ```

------------------

**Find a Single Account by a Unique Key**
----

Find account by a unique key - username, phone number, personal id or email. Key can be any column in the `accounts` table but the return value is always the first match (not an array).

* **URL**

    /findByKeyAndValue/:key/:value

* **Method:**

    `GET`
  
* **Query Params**

    ```
    key: string (username | phoneNumber | personalId | email)
    value: string
    ```

* **Request example**

    ```
    /findByKeyAndValue/phoneNumber/123123123

    OR

    /findByKeyAndValue/email/asd@asd.asd
    ```



* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 

    ```
    {
        "data": {
            "id": 17,
            "username": null,
            "email": null,
            "phoneNumber": "123123123",
            "personalId": "11223344556",
            "abUserId": null,
            "firstName": "testo",
            "lastName": "testy",
            "birthDate": "2010-10-10",
            "gender": "FEMALE",
            "citizenship": "GEORGIA",
            "city": "Quteisi",
            "status": "UNVERIFIED",
            "isPoliticallyExposedPerson": false,
            "isEntrepreneur": false,
            "comment": null,
            "isBlocked": null,
            "additionalInfo": null,
            "createdAt": "2019-06-06T12:01:12.559Z",
            "updatedAt": "2019-06-06T12:42:19.032Z"
        }
    }
    ```
      if no account was found:

    ```
    {
        "data": null
    }
    ```
 
* **Error Response:**

  * **Code:** 200 OK <br />
    **Content:** 

    ```
    {
        "service_error": {
            "type": "SYSTEM_ERROR",
            "httpCode": 500,
            "additionalData": "SequelizeDatabaseError: column account.some_random_key does not exist"
        }
    }
    ```
------------------

**Get List of Accounts (with specified fields)**
----

* **URL**

  /getAccountList

* **Method:**

  `POST`
  
* **Data Params**
    <br>
    **Required:**
    <br>
    ```
    ids = [number] //array of id's
    fields = [string] //array of fields to select from accounts table
    ```

* **Request example**

  ```
  /getAccountList
  
  body:

  {
	"ids": [1, 5],
	"fields": ["id", "phoneNumber"]
  }
  ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
      ```
    {
        "data": [
            {
                "id": 5,
                "phoneNumber": "599180093"
            },
            {
                "id": 1,
                "phoneNumber": "574938893"
            }
        ]
    }
      ```

* **Error Response:**

  * **Code:** 200 OK <br />
    **Content:** 

    ```
    {
        "service_error": {
            "type": "VALIDATION_ERROR",
            "statusCode": 400,
            "description": "VALIDATION_ERROR"
            ...
        }
    }
    ```

------------------

**Set Username**
----

* **URL**

  /accounts/:id/username

* **Method:**

  `POST`

  
* **URL Params**
    <br>
    **Required:**
    <br>
    ```
       id = [number] -- account id
    ```
  
* **Data Params**
    <br>
    **Required:**
    <br>
    ```  
    username: string
    ```

* **Request example**

  ```
  /accounts/17/username
  
  body:

  {
      username: "testusername"
  }
  ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
      ```
    {
        "data": [
            1
        ]
    }
      ```

* **Error Response:**

  * **Code:** 200 OK <br />
    **Content:** 

    ```
    {
        "service_error": {
            "type": "LOGICAL_ERROR",
            "httpCode": 400,
            "description": "ACCOUNT_NOT_FOUND"
        }
    }
    ```

    OR

    ```
    {
        "service_error": {
            "type": "LOGICAL_ERROR",
            "httpCode": 400,
            "description": "USERNAME_ALREADY_SET"
        }
    }
    ```

------------------

**Update Username**
----

* **URL**

  /accounts/:id/username

* **Method:**

  `PUT`
  
* **URL Params**
    <br>
    **Required:**
    <br>
    ```
       id = [number] -- account id
    ```
  
* **Data Params**
    <br>
    **Required:**
    <br>
    ```  
    username: string
    ```

* **Request example**

  ```
  /accounts/17/username
  
  body:

  {
      username: "testusername"
  }
  ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```
    {
        "data": [
            1
        ]
    }
    ```

* **Error Response:**

  * **Code:** 200 OK <br />
    **Content:** 

    ```
    {
        "service_error": {
            "type": "LOGICAL_ERROR",
            "httpCode": 400,
            "description": "USERNAME_UNAVAILABLE"
        }
    }
    ```

------------------

**Update Phone Number**
----

* **URL**

  /accounts/:id/phoneNumber

* **Method:**

  `PUT`
  
* **URL Params**
    <br>
    **Required:**
    <br>
    ```
       id = [number] -- account id
    ```
  
* **Data Params**
    <br>
    **Required:**
    <br>
    ```  
    phoneNumber: string
    ```

* **Request example**

  ```
  /accounts/17/phoneNumber
  
  body:

  {
      phoneNumber: "555555555"
  }
  ```
   

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```
    {
        "data": [
            1
        ]
    }
    ```

* **Error Response:**

  * **Code:** 200 OK <br />
    **Content:** 

    ```
    {
        "service_error": {
            "type": "LOGICAL_ERROR",
            "httpCode": 400,
            "description": "PHONE_UNAVAILABLE"
        }
    }
    ```

------------------

**Update Personal ID**
----

* **URL**

  /accounts/:id/personalId

* **Method:**

  `PUT`
  
* **URL Params**
    <br>
    **Required:**
    <br>
    ```
       id = [number] -- account id
    ```
  
* **Data Params**
    <br>
    **Required:**
    <br>
    ```  
    personalId: string
    ```

* **Request example**

  ```
  /accounts/17/personalId
  
  body:

  {
      personalId: "555555555"
  }
  ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```
    {
        "data": [
            1
        ]
    }
    ```

* **Error Response:**

  * **Code:** 200 OK <br />
    **Content:** 

    ```
    {
        "service_error": {
            "type": "LOGICAL_ERROR",
            "httpCode": 400,
            "description": "PERSONAL_ID_UNAVAILABLE"
        }
    }
    ```

------------------

**Change Email Status**
----

* **URL**

  /accounts/:id/emailStatus

* **Method:**

  `PUT`

* **URL Params**
    <br>
    **Required:**
    <br>
    ```
       id = [number] -- account id
    ```
   
* **Data Params**
    <br>
    **Required:**
    <br>
    ```    
    status: string ('VERIFIED' | 'UNVERIFIED')
    ```

* **Request example**

  ```
  /accounts/17/emailStatus
  
  body:

  {
      status: "VERIFIED"
  }
  ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```
    {
        "data": [
            1
        ]
    }
    ```

* **Error Response:**

  * **Code:** 200 OK <br />
    **Content:** 

    ```
    {
        "service_error": {
            "type": "LOGICAL_ERROR",
            "httpCode": 400,
            "description": "ACCOUNT_NOT_FOUND"
        }
    }
    ```

------------------

**Update Account Email**
----

* **URL**

  /accounts/:id/email

* **Method:**

  `PUT`
  
* **URL Params**
    <br>
    **Required:**
    <br>
    ```
       id = [number] -- account id
    ```
  
* **Data Params**
    <br>
    **Required:**
    <br>
    ```  
    email: string
    ```

* **Request example**

  ```
  /accounts/17/personalId
  
  body:

  {
      email: "test@adjarabet.com"
  }
  ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```
    {
        "data": [
            1
        ]
    }
    ```

* **Error Response:**

  * **Code:** 200 OK <br />
    **Content:** 

    ```
    {
        "service_error": {
            "type": "LOGICAL_ERROR",
            "httpCode": 400,
            "description": "EMAIL_UNAVAILABLE"
        }
    }
    ```

------------------

**Update Account Status**
----

* **URL**

  /accounts/:id/status

* **Method:**

  `PUT`
    
* **URL Params**
    <br>
    **Required:**
    <br>
    ```
       id = [number] -- account id
    ```
  
* **Data Params**
    <br>
    **Required:**
    <br>
    ```    
    status: string ('VERIFIED' | 'VERIFICATION_PENDING' | 'UNVERIFIED' | 'EXPIRED')
    ```

* **Request example**

    ```
     /accounts/17/status
  
       body:
        {      
            "status": "VERIFIED"
        }
    ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```
    {
        "data": [
            1
        ]
    }
    ```

* **Error Response:**

  * **Code:** 200 OK <br />
    **Content:**

    ```
    {
        "service_error": {
            "type": "LOGICAL_ERROR",
            "httpCode": 400,
            "description": "ACCOUNT_NOT_FOUND"
        }
    }
    ```

------------------

**Set Account BockStatus**
----

* **URL**

  /accounts/:id/blockStatus

* **Method:**

  `PUT`

 * **URL Params**
    <br>
    **Required:**
    <br>
    ```
       id = [number] -- account id
    ```
  
  
* **Data Params**
    <br>
    **Required:**
    <br>
    ```   
    status: boolean
    ```
    **Optional:**
    <br>
    ```
    comment: string
    ```

* **Request example**

    ```
  /accounts/17/blockStatus
  
    body:
    {       
        "status": true
        "comment": "lafsha ti padskalznulsya"
    }
    ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```
    {
        "data": [
            1
        ]
    }
    ```

* **Error Response:**

  * **Code:** 200 OK <br />
    **Content:**

    ```
    {
        "service_error": {
            "type": "LOGICAL_ERROR",
            "httpCode": 400,
            "description": "ACCOUNT_NOT_FOUND"
        }
    }
    ```

------------------

**Add Document**
----

* **URL**

  /accountDocuments

* **Method:**

  `POST`
  
* **Data Params**
    <br>
    **Required:**
    <br>
    ```
    documentType: string ('ID_CARD' | 'PASSPORT')
    documentId: string
    documentIssueDate: string (date)
    documentExpirationDate: string (date)
    accountId: number
    fileType: string ('FRONT' | 'BACK' | 'PAGE' | 'BUNDLE')
    fileName: string
    groupKey: string
    ```

* **Request example**

    ```
    [{
        "documentType":"PASSPORT",
        "documentId":"1",
        "documentIssueDate":"01-01-2019",
        "documentExpirationDate":"01-01-2025",
        "accountId":"17",
        "fileType":"FRONT",
        "fileName":"IMG_20180813_201449.jpg",
        "groupKey":"ee45261f-8580-4b71-83b9-f600c1f147b1"
    }]
    ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```
    {
        "data": [
            {
                "id": null,
                "accountId": "17",
                "documentId": "1",
                "documentType": "PASSPORT",
                "documentIssueDate": "2019-01-01",
                "documentExpirationDate": "2025-01-01",
                "status": "VERIFICATION_PENDING",
                "fileName": "IMG_20180813_201449.jpg",
                "fileType": "FRONT",
                "groupKey": "ee45261f-8580-4b71-83b9-f600c1f147b1",
                "createdAt": "2019-06-10T12:07:21.345Z",
                "updatedAt": "2019-06-10T12:07:21.345Z"
            }
        ]
    }
    ```

* **Error Response:**

  * **Code:** 200 OK <br />
    **Content:**

    ```
    {
        "service_error": {
            "type": "SYSTEM_ERROR",
            "httpCode": 500,
            "additionalData": "SequelizeForeignKeyConstraintError: insert or update on table \"documents\" violates foreign key constraint \"documents_accountId_fkey\""
        }
    }
    ```

------------------

**Update Document by groupKey**
----

* **URL**

  /accountDocuments/:groupKey

* **Method:**

  `PUT`
  
 * **URL Params**
    <br>
    **Required:**
    <br>
    ```
    groupKey = [number] -- document group key
    ```
  

* **Data Params**
    <br>
    **Required:**
    <br>
    ```
    documentType: string ('ID_CARD' | 'PASSPORT')
    documentId: string
    documentIssueDate: string (date)
    documentExpirationDate: string (date)
    accountId: number
    groupKey: string
    ```
    **Optional:**
    <br>
    status: string ('VERIFIED' | 'VERIFICATION_PENDING' | 'REJECTED' | 'EXPIRED')
    comment: string

* **Request example**

    ```
    /accountDocuments/ee45261f-8580-4b71-83b9-f600c1f147b1
  
    body:

    {
        "accountId": 17,
        "documentId": "1",
        "documentType": "PASSPORT",
        "documentIssueDate":"01-01-2019",
        "documentExpirationDate":"01-01-2025",
        "comment": null,
        "status": "VERIFICATION_PENDING",
        "groupKey":"ee45261f-8580-4b71-83b9-f600c1f147b1"
    }
    ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```
    {
        "data": [
            3
        ]
    }
    ```

* **Error Response:**

  * **Code:** 200 OK <br />
    **Content:**

    ```
    {
        "service_error": {
            "type": "SYSTEM_ERROR",
            "httpCode": 500,
            "additionalData": "SequelizeForeignKeyConstraintError: insert or update on table \"documents\" violates foreign key constraint \"documents_accountId_fkey\""
        }
    }
    ```

------------------

**Update Expired Document (used by job-manager only)**
----

Job-manager in regular intervals (e.g. daily) selects documents that should have expired according to `documentExpirationDate` and updates their status to `EXPIRED` in the database.

* **URL**

  /accountDocuments/expire

* **Method:**

  `PUT`
  
* **Data Params**
    none


* **Success Response:**

  * **Code:** 200 <br />
    **No Content**

------------------

**Automatically Update Account Status**
----

After a new document is uploaded the account `status` may or may not change. This endpoint selects the account's documents from database and calculates the correct account `status` derived from his/her document `status`'s.
<br />
It then updates the account `status` (only if necessary).

* **URL**

  /checkAccountStatusChangeOnDocumentUpload

* **Method:**

  `POST`
  
* **Data Params**
    <br>
    **Required:**
    <br>
    ```
    accountId: number
    ```

* **Request example**

    ```
    {
        "accountId":"17"
    }
    ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```
    {
        "data": "VERIFICATION_PENDING"
    }
    ```

* **Error Response:**

  * **Code:** 200 OK <br />
    **Content:**

    ```
    {
        "service_error": {
            "type": "LOGICAL_ERROR",
            "httpCode": 400,
            "description": "ACCOUNT_NOT_FOUND"
        }
    }
    ```

------------------

**GET ACCOUNT DOCUMENTS**
----

* **URL**

  /accountDocuments/:id

* **Method:**

  `GET`
  
* **Request Params**
    <br>
    **Required:**
    <br>
    id: number
    **Optional Query Parms**
    withFiles: 'true' | 'false' (true returns one object per file, false returns one object per document)
    excludeRejected: 'true' | 'false'

* **Request example**
    /accountDocuments/17?withFiles=true&excludeRejected=true

* **Success Response:** 
  * **Code:** 200 <br />
    **Content:** 
    
    ```
    {
        "data": [
            {
                "id": 54,
                "accountId": 17,
                "documentId": "1",
                "documentType": "PASSPORT",
                "documentIssueDate": "2019-01-01",
                "documentExpirationDate": "2025-01-01",
                "comment": null,
                "status": "VERIFICATION_PENDING",
                "fileName": "IMG_20180813_201449.jpg",
                "fileType": "FRONT",
                "groupKey": "ee45261f-8580-4b71-83b9-f600c1f147b1",
                "createdAt": "2019-06-10T12:03:42.454Z",
                "updatedAt": "2019-06-10T12:13:06.345Z"
            },
            {
                "id": 55,
                "accountId": 17,
                "documentId": "1",
                "documentType": "PASSPORT",
                "documentIssueDate": "2019-01-01",
                "documentExpirationDate": "2025-01-01",
                "comment": null,
                "status": "VERIFICATION_PENDING",
                "fileName": "IMG_20180813_201449.jpg",
                "fileType": "FRONT",
                "groupKey": "ee45261f-8580-4b71-83b9-f600c1f147b1",
                "createdAt": "2019-06-10T12:05:45.035Z",
                "updatedAt": "2019-06-10T12:13:06.345Z"
            },
            {
                "id": 56,
                "accountId": 17,
                "documentId": "1",
                "documentType": "PASSPORT",
                "documentIssueDate": "2019-01-01",
                "documentExpirationDate": "2025-01-01",
                "comment": null,
                "status": "VERIFICATION_PENDING",
                "fileName": "IMG_20180813_201449.jpg",
                "fileType": "FRONT",
                "groupKey": "ee45261f-8580-4b71-83b9-f600c1f147b1",
                "createdAt": "2019-06-10T12:07:21.345Z",
                "updatedAt": "2019-06-10T12:13:06.345Z"
            }
        ]
    }
    ```

 -----------------

 **Get Verification Queue**
----

Select documents with filters and pagination. Used for back-office to let administrators view verification queue (all the documents with `VERIFICATION_PENDING` statuses)

* **URL**

  /verificationQueue

* **Method:**

  `GET`
  
* **Data Params**

    **Optional Query Params:**
    <br>
    ```
    from: string,
    to: string,
    accountId: string,
    personalId: string,
    documentStatus: string ('VERIFIED' | 'VERIFICATION_PENDING' | 'EXPIRED' | 'REJECTED')

    page: number (default=0) --paging
    pageSize: number (default=10) --paging
    ```

* **Request example**

    ```
    /verificationQueue?from=2019-05-10&to=2019-05-19&personalId=12312312312&documentStatus=VERIFICATION_PENDING&page=0&pageSize=10
    ```

* **Success Response:**

  * **Code:** 200 <br />
    **Content:**
    ```
    {
        "data": {
            "data": [
                {
                    "groupKey": "ee45261f-8580-4b71-83b9-f600c1f147b1",
                    "documentCreatedAt": "2019-06-10T12:07:21.345Z",
                    "documentId": "1",
                    "documentType": "PASSPORT",
                    "documentStatus": "VERIFICATION_PENDING",
                    "accountId": 17,
                    "documentComment": null,
                    "account.personalId": "12312312312"
                }
            ],
            "count": 1
        }
    }
    ```

* **Error response:**

    Validation Error
```
{
    "service_error": {
        "name": "VALIDATION_ERROR",
        "statusCode": 400,
        "type": "VALIDATION_ERROR",
        "description": "ValidationError",
        "additionalData": [
            {
                "message": "\"documentStatus\" must be one of [VERIFIED, VERIFICATION_PENDING, EXPIRED, REJECTED]",
                "path": [
                    "documentStatus"
                ],
                "type": "any.allowOnly",
                "context": {
                    "value": "dsa",
                    "valids": [
                        "VERIFIED",
                        "VERIFICATION_PENDING",
                        "EXPIRED",
                        "REJECTED"
                    ],
                    "key": "documentStatus",
                    "label": "documentStatus"
                }
            }
        ]
    }
}
```

------------------